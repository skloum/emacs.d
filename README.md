# .emacs.d

Mes fichiers de configuration pour Emacs

## Clonage de la configuration

Dans le homedir :
- avec authentification ssh pour pouvoir pousser des modifications : `git clone --recurse-submodules git@gitlab.com:skloum/emacs.d.git .emacs.d`
- en https : `git clone --recurse-submodules https://gitlab.com/skloum/emacs.d.git .emacs.d`

Avant la première utilisation, il faut exécuter le script `tangle-emacs-init` pour générer les fichiers ".el".
