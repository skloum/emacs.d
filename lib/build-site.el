(require 'ox-publish)

(setq org-html-validation-link nil            ;; Pas de création de lien de validation W3
      org-export-with-sub-superscripts nil    ;; Affichage correct des _ pour l'export HTML
      org-html-htmlize-output-type 'css       ;; Utilisation d'un css externe pour la coloration syntaxique des blocs de code
      org-html-head-extra "<link rel=\"stylesheet\" href=\"css/org-src-doom-palenight.css\" />")

(setq org-publish-project-alist
      '(;; Sortie «standard»
        ("org"
         :base-directory "."
         :publishing-function org-html-publish-to-html
         :publishing-directory "./public"
         :htmlized-source t
         :language "fr")

        ("src"
         :recursive t
         :base-directory "./src"
         :base-extension "css\\|js"
         :publishing-function org-publish-attachment
         :publishing-directory "./public/src")

        ("css"
         :base-directory "./lib/css"
         :base-extension "css"
         :publishing-function org-publish-attachment
         :publishing-directory "./public/css")))
(org-publish-all t)

(make-symbolic-link "emacs-init.html" "./public/index.html" t)

(message "Build complete!")
